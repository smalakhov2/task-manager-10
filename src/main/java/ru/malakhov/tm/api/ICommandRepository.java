package ru.malakhov.tm.api;

import ru.malakhov.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();


}
