package ru.malakhov.tm.api;

public interface ICommandController {

    void showInfo();

    void exit();

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

}
