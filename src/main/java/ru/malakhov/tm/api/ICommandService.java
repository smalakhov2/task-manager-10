package ru.malakhov.tm.api;

import ru.malakhov.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
