package ru.malakhov.tm.bootstrap;

import ru.malakhov.tm.api.ICommandController;
import ru.malakhov.tm.api.ICommandRepository;
import ru.malakhov.tm.api.ICommandService;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.controller.CommandController;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.service.CommandService;


import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String args[]) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            final String command = scanner.nextLine();
            parseCommand(command);
        }

    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                System.out.println("Unknow command.");
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                parseArg(arg);
                break;
        }
    }

}
